/* 
**  All the common js methods are called here
*/
import select2 from 'select2';
import datepicker from 'js-datepicker'

export default class AppCommon {
    constructor() {
        this.init();
    }

    // init the class
    init() {
        this.formSelect();
        this.formDate();
        this.formHandling();
    }

    // trigger the select2 for the form
    formSelect() {
        // do ajax query to the endpoint using the dynamic url process of selec2 and then process the results as needed for select2 to render the list
        // using the API from https://github.com/apilayer/restcountries
        $('#country').select2({
            placeholder: 'Choose Country',
            minimumInputLength: 2,
            dropdownParent: $('#selectcountry'),
            ajax: {
                url: params => {
                    // the api needs the param sent in the url
                    return 'https://restcountries.eu/rest/v2/name/' + params.term;
                },
                processResults: response => {
                    if (!response) {
                        return {results: []};
                    }
                    return {
                        results: response.map(data => {
                            return {
                                text: data.name,
                                id: data.name,
                            }
                        })
                    };
                },
            },
            language: {
                // this was hack as the api endpoint doesnt provide any error handling
                errorLoading: () => {
                    return 'No countries found';
                }
            }
        });
    }

    // initialize the custom datepicker
    formDate() {
        datepicker('.datepicker', {
            maxDate: new Date(),
            overlayButton: 'Submit Year',
            formatter: (input, date, instance) => {
                const value = date.toLocaleDateString()
                input.value = value // => '1/1/2099'
            }
        })
    }

    // do the form handling here
    formHandling() {
        let formarea = $('.form-render'),
            wpcf7Elm = formarea.find('.wpcf7'),
            form = formarea.find('.wpcf7-form'),
            response = form.find('.response-wrap'),
            btn = form.find('.btn[type="submit"]');

        wpcf7Elm.on('wpcf7submit', function (e) {
            // show the respnonse message and add disabled class to button
            response.fadeIn();
            btn.addClass('disabled');

            // adding timeout to close the response message for form
            let timeOut = 3000;

            if (form.hasClass('sent')) {
                timeOut = 5000;
                // reset the select2 
                $('#country').val('').trigger('change');
            }

            setTimeout(() => {
                btn.removeClass('disabled');
                response.fadeOut('slow');
            }, timeOut);
        });
    }
}