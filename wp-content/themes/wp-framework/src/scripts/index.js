/*!
*   Title: Ajasra's Custom Wp Theme
*   Description: All custom javascript overrides are here.
*   Version: 3.0.2
*   Author: Ajasra Das(das.ajasra@gmail.com)
*/

let $ = jQuery.noConflict();

if (typeof jQuery != 'undefined') {
    console.log('jQuery ', jQuery.fn.jquery);
}

import ready from 'domready';

import AppCommon from './AppCommon';

ready(() => {
    new AppCommon();
});