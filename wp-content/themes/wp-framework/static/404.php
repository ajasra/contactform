<?php
// default template for the 404 page 
get_header();
?>

<div class="single-page single-404">
    
    <section class="section-error">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-md-10 col-lg-8 text-center">
                    <figure>
                        <img src="<?php echo image('404.svg');?>" alt="404">
                    </figure>
                    <h1 class="title"><?php _e('Page not found!', 'aja21'); ?></h1>
                    <a href="javascript:history.go(-1)" class="btn btn-primary">
                        <?php _e('Go Back', 'aja21'); ?>
                    </a>
                </div>
            </div>
        </div>
    </section>

</div>

<?php
get_footer();