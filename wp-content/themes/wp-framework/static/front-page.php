<?php
/*
	Template name: Homepage
*/
get_header();

// do the loop
if (have_posts()) :
	while (have_posts()) : the_post();
	?>

	<div class="single-page single-home">
		<section class="section-form formwrap">
			<div class="formwrap__inner">
				<?php
				// since blocks are used just print the content
				the_content();
				?>
			</div>
		</section>
	</div>

	<?php
	endwhile;
endif;
wp_reset_postdata();
get_footer();
