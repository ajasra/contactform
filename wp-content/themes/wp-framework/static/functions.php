<?php
// installer
require_once(get_template_directory().'/functions/install-activate.php');

// Required plugins
require_once(get_template_directory().'/functions/required_plugins.php');

// WP Head and other cleanup functions
require_once(get_template_directory().'/functions/cleanup.php');

// Register scripts and stylesheets
require_once(get_template_directory().'/functions/enqueue-scripts.php');

// Image size
require_once(get_template_directory().'/functions/image.php');

// Cpt
require_once(get_template_directory().'/functions/cpt.php');

// Metabox
require_once(get_template_directory().'/functions/metabox.php');

// Custom theme functions
require_once(get_template_directory().'/functions/theme-functions.php');

// CF7
require_once(get_template_directory().'/functions/contact-form.php');
