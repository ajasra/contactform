<?php
/* ========================================================================================================== */
/* METABOXES (meta-box plugin)
/* ========================================================================================================== */
add_filter('rwmb_meta_boxes', 'aja21_register_meta_boxes');

function aja21_register_meta_boxes($meta_boxes) {
    $prefix = 'aja_';

    // Home page
    // $meta_boxes[] = array(
    //     'id'            => 'homePageMb',
    //     'title'         => __('Additional content for the page goes here', 'aja21'),
    //     'post_types'    => array('page'),
    //     'context'       => 'normal',
    //     'priority'      => 'high',
    //     'autosave'      => true,
    //     'include'       => array(
    //         'template'  => array('front-page.php'),
    //     ),
    //     'fields'        => array(
    //         array(
    //             'name'  => __('Contact Form Shortcode', 'aja21'),
    //             'id'    => $prefix . 'contact_shortcode',
    //             'type'  => 'text',
    //             'size'  => 100,
    //             'desc'  => __('Add the shortcode from CF7 to display the contact form in the home page', 'aja21')
    //         ),
    //     ),
    // );

    $meta_boxes[] = array(
        'id'            => 'formDataCptMb',
        'title'         => __('Form Information', 'jtlb'),
        'post_types'    => array('form-data'),
        'content'       => 'normal',
        'priority'      => 'high',
        'autosave'      => true,
        'fields'        => array(
            array(
                'name'      => __('Fisrt Name', 'jtlb'),
                'id'        => $prefix . 'firstname',
                'type'      => 'text',
                'size'      => 100
            ),
            array(
                'name'      => __('Last Name', 'jtlb'),
                'id'        => $prefix . 'lastname',
                'type'      => 'text',
                'size'      => 100
            ),
            array(
                'name'      => __('Email', 'jtlb'),
                'id'        => $prefix . 'email',
                'type'      => 'text',
                'size'      => 100
            ),
            array(
                'name'      => __('Phone', 'jtlb'),
                'id'        => $prefix . 'phone',
                'type'      => 'text',
                'size'      => 100
            ),
            array(
                'name'      => __('Country', 'jtlb'),
                'id'        => $prefix . 'country',
                'type'      => 'text',
                'size'      => 100
            ),
            array(
                'name'      => __('DOB', 'jtlb'),
                'id'        => $prefix . 'dob',
                'type'      => 'text',
                'size'      => 100
            ),
        )
    );

    return $meta_boxes;
}
