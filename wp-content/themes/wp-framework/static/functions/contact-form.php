<?php
/* 
    Everything related to cf7 goes here
*/

// Remove <p> and <br/> from cf7
add_filter('wpcf7_autop_or_not', '__return_false');


// Refactor the cf7 block to have a custom wrapper so that the page thumbnail can be plugged inside it
add_filter('render_block', 'aja21_refactor_blocks_markup', 10, 2 );
function aja21_refactor_blocks_markup( $block_content, $block ) {
    if (!is_admin() && is_front_page()) {
        if ($block['blockName'] == 'contact-form-7/contact-form-selector') {
            $inner_html = $block['innerHTML'];
            
            global $post;
            $thumb = get_the_post_thumbnail_url( $post, 'contactform' );

            $return_data = "
            <div class='form-render'>
                {$inner_html}
                <figure>
                    <img src='{$thumb}' class='img-fluid'>
                </figure>
            </div>
            ";
            return $return_data;
        }
        return $block_content;
    }
}


/* 
    ** Create a custom handler for fetching and parsing the date mask.
    ** call the wpcf7_add_form_tag from a hook to respond to the tag creation handler on on  form init
*/
// create the handler
function aja21_custom_form_tags_handler($tag) {
    // get the tag
    $tag = new WPCF7_FormTag($tag);

    if (empty($tag->name)) {
        return '';
    }
    // check for validation error 
    $validation_error = wpcf7_get_validation_error($tag->name);
    // fetch the classes from the shortcode
    $class = wpcf7_form_controls_class($tag->type);

    // check if there is validation error -- if yes add the error class
    if ($validation_error) {
        $class .= ' wpcf7-not-valid';
    }

    // create the attributes for the custom form field
    $atts = array();

    // these are the attributes to fetched from the shortcode (weird that its not there in the docs)
    $atts['class'] = $tag->get_class_option($class);
    $atts['id'] = $tag->get_id_option();

    if ( $tag->has_option('readonly')) {
        $atts['readonly'] = 'readonly';
    }
    // add the cf7 required params
    if ($tag->is_required()) {
        $atts['aria-required'] = 'true';
    }

    $atts['aria-invalid'] = $validation_error ? 'true' : 'false';

    $atts['name'] = $tag->name;

    if ($tag->basetype == 'ajadate') {
        // add custom attributes to field
        $atts['type'] = 'text';
        $atts['placeholder'] = (string)reset($tag->values);
        $atts['readonly'] = 'readonly';
    }
    

    $atts = wpcf7_format_atts($atts);

    switch ($tag->basetype) {
        case 'ajadate':            
            $html = sprintf(
            '<span class="wpcf7-form-control-wrap %1$s"><input %2$s /><span class="icon icon-calendar"></span>%3$s</span>',
            sanitize_html_class( $tag->name ), $atts, $validation_error );    
    
            return $html;
            break;
            
        case 'ajacountries':
            $html = '<option value=""></option>';

            $html = sprintf(
                    '<span class="wpcf7-form-control-wrap %1$s" id="select%1$s"><select %2$s>%3$s</select>%4$s</span>',
                    sanitize_html_class( $tag->name ),
                    $atts,
                    $html,
                    $validation_error); 
    
            return $html;
            break;
    }
}

// wpcf7-form-control wpcf7-select wpcf7-validates-as-required
// call the form tag
add_action('wpcf7_init', 'aja21_custom_add_form_tags');

function aja21_custom_add_form_tags() {
    wpcf7_add_form_tag(array(
        'ajadate',
        'ajadate*',
        'ajacountries',
        'ajacountries*',
    ), 'aja21_custom_form_tags_handler', true);
}

// validate the custom form field
add_filter( 'wpcf7_validate_ajadate*', 'aja21_cf7_custom_validations', 20, 2 );
add_filter( 'wpcf7_validate_ajacountries', 'aja21_cf7_select_validation_filter', 20, 2 );
add_filter( 'wpcf7_validate_ajacountries*', 'aja21_cf7_select_validation_filter', 20, 2 );

function aja21_cf7_custom_validations($result, $tag) {
    $name = $tag->name;

	$value = isset( $_POST[$name] )
		? trim( strtr( (string) $_POST[$name], "\n", " " ) )
		: '';

	if ( $tag->is_required() and '' == $value ) {
		$result->invalidate( $tag, wpcf7_get_message( 'invalid_required' ) );
	} elseif ( '' != $value and ! aja21_check_date_is_valid( $value ) ) {
		$result->invalidate( $tag, wpcf7_get_message( 'invalid_date' ) );
    }
    
    return $result;
}

function aja21_cf7_select_validation_filter( $result, $tag ) {
	$name = $tag->name;

	if ( isset( $_POST[$name] )
	and is_array( $_POST[$name] ) ) {
		foreach ( $_POST[$name] as $key => $value ) {
			if ( '' === $value ) {
				unset( $_POST[$name][$key] );
			}
		}
	}

	$empty = ! isset( $_POST[$name] ) || empty( $_POST[$name] ) && '0' !== $_POST[$name];

	if ( $tag->is_required() and $empty ) {
		$result->invalidate( $tag, wpcf7_get_message( 'invalid_required' ) );
	}

	return $result;
}

// validation for date to trigger error if needed
function aja21_check_date_is_valid($date) {
    $date_parts = explode('/', $date);

    if (checkdate( $date_parts[1], $date_parts[0], $date_parts[2] )){
        return true;
    } else{
        return false;
    }
}



/* 
    Save the contact form submission data to the db as cpt        
*/
add_action('wpcf7_before_send_mail', 'aja21_custom_doc_cf7_process_form');
function aja21_custom_doc_cf7_process_form($cf7) {
    $submission = WPCF7_Submission::get_instance();

    if ( $submission ) {
        $posted_data = $submission->get_posted_data();
    }

    // check if the primary form 
    // todo the form can be added to theme options to make this process more dynamic if needed
    $primary_form = [7];
    if (in_array($cf7->id(), $primary_form)) {
        // insert the post in the leader cpt
        $post_id = wp_insert_post(array(
            'post_status'   => 'private',
            'post_title'    => $posted_data['firstname'] . ' ' . $posted_data['lastname'],
            'post_type'     => 'form-data', //Post Type
        ));

        // insert the metabox data
        if (!is_wp_error($post_id)) {
            update_post_meta($post_id, 'aja_firstname', $posted_data['firstname']);
            update_post_meta($post_id, 'aja_lastname', $posted_data['lastname']);
            update_post_meta($post_id, 'aja_email', $posted_data['emailId']);
            update_post_meta($post_id, 'aja_phone', $posted_data['phone']);
            update_post_meta($post_id, 'aja_country', $posted_data['country']);
            update_post_meta($post_id, 'aja_dob', $posted_data['dob']);
        }
    }
}