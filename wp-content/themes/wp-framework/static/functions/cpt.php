<?php
/* 
    Everything about custom posts go here
*/

if (function_exists('register_post_type')) {

    $labels = array(
        'name'              => _x('Form Data', 'post type general name', 'jtlb'),
        'singular_name'     => _x('Form Data', 'post type singular name', 'jtlb'),
        'add_new'           => _x('Add new Form Data', 'Form Data', 'jtlb'),
        'add_new_item'      => __('Add new Form Data', 'jtlb'),
        'edit_item'         => __('Modify Form Data', 'jtlb'),
        'new_item'          => __('New Form Data', 'jtlb'),
        'view_item'         => __('View Form Data', 'jtlb'),
        'search_items'      => __('Search Form Data', 'jtlb'),
        'not_found'         => __('No Form Data found', 'jtlb'),
        'not_found_in_trash' => __('No Form Data found in trash', 'jtlb'),
        'parent_item_colon' => '',
        'menu_name'         => 'Form Data'
    );
    $args = array(
        'labels'                => $labels,
        'public'                => true,
        'publicly_queryable'    => false,
        'exclude_from_search'   => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'query_var'             => false,
        'rewrite'               => true,
        'capability_type'       => 'post',
        'has_archive'           => true,
        'menu_icon'             => 'dashicons-forms',
        'hierarchical'          => false,
        'menu_position'         => 20,
        'supports'              => array('title')
    );
    register_post_type('form-data', $args);

}



// Add the custom columns to the form-data post type:
add_filter('manage_form-data_posts_columns', 'aja21__set_form_data_edit_cpt_columns');
function aja21__set_form_data_edit_cpt_columns($columns) {
    $date = $columns['date'];
    unset($columns['date']);

    $columns['email'] = __('Email', 'jtlb');
    $columns['phone'] = __('Phone', 'jtlb');
    $columns['country'] = __('Country', 'jtlb');
    $columns['dob'] = __('DOB', 'jtlb');
    $columns['date'] = $date;

    return $columns;
}

// Add the data to the custom columns for the form-data post type:
add_action('manage_form-data_posts_custom_column', 'aja21__form_data_custom_cpt_column', 10, 2);
function aja21__form_data_custom_cpt_column($column, $post_id) {
    switch ($column) {
        case 'email':
            $meta = get_post_meta($post_id, 'aja_email', true);
            echo $meta;
            break;
        case 'phone':
            $meta = get_post_meta($post_id, 'aja_phone', true);
            echo $meta ? $meta : '-';
            break;
        case 'country':
            $meta = get_post_meta($post_id, 'aja_country', true);
            echo $meta ? $meta : '-';
            break;
        case 'dob':
            $meta = get_post_meta($post_id, 'aja_dob', true);
            echo $meta ? $meta : '-';
            break;
    }
}