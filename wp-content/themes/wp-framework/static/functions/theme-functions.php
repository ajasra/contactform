<?php

// enqueue dashicons style for frontend use
add_action('admin_enqueue_scripts', 'aja21_enqueue_dashicons');
function aja21_enqueue_dashicons() {
    wp_enqueue_style('dashicons');
}

// hide admin bar
// show_admin_bar(false);


// POST THUMBNAIL
add_theme_support('post-thumbnails');

// custom backend footer
add_filter('admin_footer_text', 'aja21_custom_admin_footer');
function aja21_custom_admin_footer() {
    _e('<span id="footer-thankyou">Developed by Ajasra Das</span>', 'aja21');
}


// quick image path
function image($name) {
    $path = get_bloginfo('template_directory') . '/assets/images/' . $name;
    return $path;
}

/* removing using of .bmp files */
function aja21_mime_types_setting($mime_types) {
    unset($mime_types['bmp']); // removing the bmp extension
    unset($mime_types['tif|tiff']); //removing the tiff extension
    /*$mime_types['avi'] = 'video/avi'; //adding avi extension*/ /*todo: this how we add new mime types*/
    $mime_types['ogv'] = 'video/ogg';
    $mime_types['webm'] = 'video/webm';
    return $mime_types;
}
add_filter('upload_mimes', 'aja21_mime_types_setting', 1, 1);


// adding favicon to admin pages
// need to make sure that function runs when you're on the login page and admin pages
add_action('login_head', 'aja21_add_favicon');
add_action('admin_head', 'aja21_add_favicon');

function aja21_add_favicon() {
    $favicon_url = image('admin-favicon.png');
    echo '<link rel="shortcut icon" type="image/x-icon" href="' . $favicon_url . '" />';
}


// adding custom logo to wp login page
add_action( 'login_enqueue_scripts', 'aja21_login_logo' );
function aja21_login_logo() { ?>
    <style type="text/css">
        #login h1 a, .login h1 a {
            background-image: url(<?php echo image('logo.png') ?>);
            height: 57px;
            width: 100%;
            background-size: contain;
            background-repeat: no-repeat;
            margin-bottom: 0px;
            pointer-events: none;
        }
    </style>
<?php }



// Limit post revisions
add_filter('wp_revisions_to_keep', 'aja21_wp_revisions_to_keep', 10, 2 );

function aja21_wp_revisions_to_keep( $num, $post ) {
    // can be set for certain post types
    // if( 'custom_post_type' == $post->post_type ) {
	//     $num = 5;
    // }
    $num = 1;
    return $num;
}



// DEBUGGER for custom rewrite rules !!
// add_action( 'wp_head', 'aja21_debug_rewrite_rules' );
function aja21_debug_rewrite_rules() {
    global $wp, $template, $wp_rewrite;

    echo '<pre style="margin: 120px 0 0 150px;">';
    echo 'Request: ';
    echo empty($wp->request) ? 'None' : esc_html($wp->request) . PHP_EOL;
    echo 'Matched Rewrite Rule: ';
    echo empty($wp->matched_rule) ? 'None' : esc_html($wp->matched_rule) . PHP_EOL;
    echo 'Matched Rewrite Query: ';
    echo empty($wp->matched_query) ? 'None' : esc_html($wp->matched_query) . PHP_EOL;
    echo 'Loaded Template: ';
    echo basename($template);
    echo '</pre>' . PHP_EOL;

    echo '<pre>';
    print_r($wp_rewrite->rules);
    echo '</pre>';
}
