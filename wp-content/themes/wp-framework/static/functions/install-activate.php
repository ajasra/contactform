<?php
/* 
    run on installation and activation of theme 
*/



/* add seed on theme activation */
add_action('after_switch_theme', 'aja21_create_seed_after_theme_switch');

function aja21_create_seed_after_theme_switch () {
    $theme_activated = get_option('aja21_theme_active');

    // do the stuff if theme is not activated or is activated for the first time
    if (!$theme_activated) :
        /* create pages */
        // home page
        $home_data = array(
            'ID'            => 2,
            'post_title'    => 'Home',
            'post_content'  => '',
            'post_type'     => 'page',
            'page_template' => 'front-page.php',
            'post_status'   => 'publish'
        );

        $home_page_id = wp_update_post($home_data);

        if ($home_page_id) {
            // set the options to change
            $option = array(
                // set home page as front page
                'page_on_front'                 => $home_page_id,
                'show_on_front'                 => 'page',
                // change category base
                'category_base'                 => '/cat',
                // change tag base
                'tag_base'                      => '/label',
                // disable comments
                'default_comment_status'        => 'closed',
                // disable trackbacks
                'use_trackback'                 => '',
                // disable pingbacks
                'default_ping_status'           => 'closed',
                // disable pinging
                'default_pingback_flag'         => '',
                // change the permalink structure
                'permalink_structure'           => '/%postname%/',
                // dont use year/month folders for uploads 
                'uploads_use_yearmonth_folders' => 1,
                // don't use those ugly smilies
                'use_smilies'                   => 0
            );
            // change the options!
            foreach ( $option as $key => $value ) {  
                update_option( $key, $value );
            }
            
            // flush rewrite rules because we changed the permalink structure
            global $wp_rewrite;
            $wp_rewrite->flush_rules();

            add_option( 'aja21_theme_active', true );
        }

    endif;    
}
