<?php
/* 
    generic template for single pages
*/
if (have_posts()) :
    while (have_posts()) : the_post();
        // just getting some basic info
        list(
            $title,
            $content,
            $feat_image
        ) = array(
            get_the_title(),
            get_the_content(),
            get_the_post_thumbnail_url(get_the_ID(), 'full')
        );
        ?>
        <div class="single-page single-default">
            <div class="container">
                <div class="row justify-content-center">
                    <?php
                    if (!empty($feat_image)) {
                        echo "
                        <div class='col-12 featured-image'>
                            <img src='{$feat_image}' class='img-fluid' alt='{$title}'>
                        </div>
                        ";
                    }
                    ?>
                    <div class="col-12 col-md-10">
                        <div class="the-content">
                            <h1 class="page-title"><?php echo $title;?></h1>
                            <?php echo $content; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php
    endwhile;
endif;
wp_reset_postdata();


