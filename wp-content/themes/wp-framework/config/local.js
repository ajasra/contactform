/* Settings for the local setup */

module.exports = {
	host: 'contactform.local', 	// directory name eg. example.com
	proxy: 'http://contactform.local/',	// project url eg. http://example.com/
	wpStyle: `
/*
	Theme Name: Ajasra's Custom Wp Theme
	Author: Ajasra Das
	Description: Custom theme framework for bootrstapping wordpress websites.
	Version: 3.0.2
	License: Commercial
	Licence: GNU General Public License v2 or later
	License URI: http://www.gnu.org/licenses/gpl-2.0.html
*/`
};