# README #
This theme framework was made for speeding up the process of theme development for wordpress websites and this is specifically part of the coding test.

## Synopsis

The theme framework is made suing Twitter Bootstrap, using webpack and NPM to manage front end tasks. Please note that this setup is made to run in MAMP. Wordpress core files in to project root, wp-admin and wp-includes directories are not included in repo. The latest database dump is included in the project root.

## Installation
1. In the project root install the wordpress v 5.7. Then remove the wp-content directory and wp-config.php file along with license.txt and readme.html.
2. Clone the repo in the root.
3. Browse to `wp-content/themes/wp-framework/config/local.js` and change the `host` and `proxy` values as in the exmample. The proxy is the URL to the project in localhost. Configure the hosts file to have `host` name bound to localhost.
4. Install node modules and create a build using the following `npm install` followed by `npm run build`.
5. Search and replace all the existence of the `proxy` in the database dump, without the trailing slashes and import it the MySQL.
6. Update the `wp-config.php` as per requriements with the table prefix set to `aja_`.
7. Login to wordpress. 

## Run the application

To run the application locally in dev mode, use
```sh
npm run start
```
To create a production build of the theme, use
```sh
npm run build
```

## Notes
The project serves the theme from `ajasra-theme` directory inside `wp-content/themes/`. There is the develepment theme that is also there in the themes direcotry - `wp-framework`. This should not be activate. This method is definitely not the correct or best way to do the development, but this was simplest way to solve my day-to-day development needs.

The build creates the theme files in `ajasra-theme` directory and this directory can entirely replaced in production/staging servers and there is no need to tamper with the development theme.

As an additional feature, the contact form data gets saved in the database as a custom post type that can be accessed from the admin section